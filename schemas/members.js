const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

var memberSchema = new schema({
    displayName: {type: String, required: true};
    userName: {type: String,  required: true, unique: true},
    password: {type: String, required: true},
    emailAddress: {type: String, required:true}
});

//schemaName.methods.methodName = function () {...}
//.save == save functionality. Goes to individual documents?
//.find == get functionality. Goes to the model??

memberSchema.pre('save', function(next) {
    var user = this;
    bcrypt.hash(user.password, null, null, function(err,hash) {
        if (err) return next(err);
        user.password = hash;
        next();
    });
});
S
memberSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var members = new mongoose.model('members', memberSchema);

module.exports = members;