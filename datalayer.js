const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/feloszt');

const db = mongoose.connection;
db.on('error',console.error.bind(console,'connection error: '));
db.once('open', function() {});

//Datalayer related functions:
var isConnected = function(err) {
    if(err) {
        console.log(`Errored connect to database. ${err}`);
        return `Errored connect to database. ${err}`;
    } else {
        console.log('Successfully connected');
        return 'Successfully connected';
    }
}


//add schemas here for the DBs
const members = require('./schemas/members');
//========all member related functions go here==========
//functions assigned to vars go here.
//var getMembers = function()....

module.exports = {
    connectStatus: isConnected
    //function variables go here.
    //getMembers : getMembers, ....
};